var searchData=
[
  ['jpeg_5fmanip_21',['JPEG_manip',['../group___j_p_e_g__manip.html',1,'']]],
  ['jpeg_5fmanip_2eh_22',['jpeg_manip.h',['../jpeg__manip_8h.html',1,'']]],
  ['jpeg_5fread_23',['jpeg_read',['../group__fonctions.html#ga00a1ecc8b13e69045ea89c454551d9c0',1,'jpeg_read(char *path):&#160;jpeg_manip.c'],['../group__fonctions.html#ga00a1ecc8b13e69045ea89c454551d9c0',1,'jpeg_read(char *path):&#160;jpeg_manip.c']]],
  ['jpeg_5fwrite_5ffrom_5fcoeffs_24',['jpeg_write_from_coeffs',['../group__fonctions.html#ga49cbcc87eb8970196839fe587d2697f8',1,'jpeg_write_from_coeffs(char *outfile, JPEGimg *img):&#160;jpeg_manip.c'],['../group__fonctions.html#ga49cbcc87eb8970196839fe587d2697f8',1,'jpeg_write_from_coeffs(char *outfile, JPEGimg *img):&#160;jpeg_manip.c']]],
  ['jpegimg_25',['JPEGimg',['../group__structures.html#ga7a579a0601424842119d1ae65e13eca2',1,'jpeg_manip.h']]],
  ['jpegimg_5fs_26',['JPEGimg_s',['../struct_j_p_e_gimg__s.html',1,'']]]
];
