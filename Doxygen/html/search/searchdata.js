var indexSectionsWithContent =
{
  0: "abcdefgijlmnprstv",
  1: "dj",
  2: "ejm",
  3: "abfgijmpr",
  4: "cdlv",
  5: "djs",
  6: "cefjst"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "groups"
};

var indexSectionLabels =
{
  0: "Tout",
  1: "Structures de données",
  2: "Fichiers",
  3: "Fonctions",
  4: "Variables",
  5: "Définitions de type",
  6: "Groupes"
};

