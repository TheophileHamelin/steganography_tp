﻿/**
 * \file main.c
 * \brief Fichier principal du TP stégano.
 * \author N.Bodin
 *
 * Complétez ce fichier pour insérer et extraire un message d'une image JPEG.
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "error.h"
#include "jpeg_manip.h"

typedef unsigned char byte;

/// @defgroup TODO
/// @brief Le travail à réaliser pour ce TP
/// @{

/// @brief Permet de modifier la valeur d'un coefficient DCT
///		   La fonction modifie le coeff de +modif
/// @param img		pointeur vers la strucutre JPEGimg contenant le tableau de coeffs DCT
/// @param pos		pointeur vers la position du coeff à modifier
/// @param modif	valeur de la modification
inline void modifDCTcoeff(JPEGimg* img, DCTpos* pos, int modif)
{

	// TODO

}


/// @brief Lecture du fichier path, récupération de la taille dans size et retour du contenu
/// @param[in]	path chemin vers le fichier à lire
/// @param[out]	size pointeur vers la taille du fichier (doit être préalablement alloué)
/// @return		un pointeur sur les données lues
byte* read_file(char* path, int* size)
{
	// TODO

	return NULL;
}

/// @brief Insertion d'un bit dans un coefficient DCT
///        L'insertion doit être du LSB replacing, rien d'autre !
/// @param img	pointeur vers la strucutre JPEGimg contenant le tableau de coeffs DCT
/// @param pos	pointeur vers la position du coeff à modifier
/// @param bit	valeur du bit à insérer
/// @return 1 si le coeff DCT a été modifié, 0 sinon
int bit_insert(JPEGimg* img, DCTpos* pos, int bit)
{
	// TODO

	return 0;
}

/// @brief Insertion d'un message dans une image JPEG
///        L'insertion doit être du LSB replacing séquentiel, rien d'autre !
///        Si le message est trop grand pour l'image, retourner la valeur ERR_TREAT
/// @param[in] msg		pointeur vers le message (tableau de unsigned char)
/// @param[in] size		taille du message (en octets)
/// @param[in,out] img	pointeur sur l'image cover
/// @return EXIT_SUCCESS ou une valeur négative en cas d'erreur
int basic_insert (byte *msg, int size, JPEGimg* img)
{
	// TODO

	return EXIT_SUCCESS;
}

/// @brief Extraction d'un message d'une image JPEG
///        L'extraction doit permettre de récupérer le message précédement inséré avec 
///        la fonction basic_insert
/// @param[in] img		pointeur vers l'image JPEG
/// @param[out] size	pointeur sur la taille du message extrait
/// @return un pointeur sur les données extraites
byte *basic_extract (JPEGimg *img, int *size)
{
	// TODO

	return NULL;
}

/// @brief Insertion d'un message dans une image JPEG avec ré-insertion si zéro
///        Si le message est trop grand pour l'image, retourner la valeur ERR_TREAT
/// @param[in] msg		pointeur vers le message (tableau de unsigned char)
/// @param[in] size		taille du message (en octets)
/// @param[in,out] img	pointeur sur l'image cover
/// @return EXIT_SUCCESS ou une valeur négative en cas d'erreur
int advanced_insert(byte* msg, int size, JPEGimg* img)
{
	// TODO

	return EXIT_SUCCESS;
}

/// @brief Extraction d'un message d'une image JPEG
///        L'extraction doit permettre de récupérer le message précédement inséré avec 
///        la fonction edvanced_insert
/// @param[in] img		pointeur vers l'image JPEG
/// @param[out] size	pointeur sur la taille du message extrait
/// @return un pointeur sur les données extraites
byte* advanced_extract(JPEGimg* img, int* size)
{
	// TODO

	return NULL;
}

/// @}

/// @brief Point d'entrée du programme
/// @param[in] argc nombre d'arguments de la ligne de commande
/// @param[in] argv arguments de la ligne de commande
/// @return EXIT_SUCCESS ou EXIT_FAILURE
int main(int argc, char** argv)
{
	int return_value;
	JPEGimg* img = NULL;
	DCTpos pos = { 0 };

	// Vérification du nombre d'arguments
	if (argc < 3)
	{
		printf("%s: Reads a jpeg image and write it in a new file\n", argv[0]);
		printf("Not enough arguments for %s\n", argv[0]);
		printf("Usage: %s <cover.jpg> <copy.jpg>\n", argv[0]);
		return EXIT_FAILURE;
	}

	// Lecture de l'image
	img = jpeg_read(argv[1]);
	if (!img)
		return EXIT_FAILURE;

	// Informations basiques
	printf("Number of components: %d\n", img->cinfo->num_components);
	for (int i = 0; i < img->cinfo->num_components; i++)
	{
		printf("Component %d:\n   %d lines, %d columns\n\n", i, img->cinfo->comp_info[i].height_in_blocks, img->cinfo->comp_info[i].width_in_blocks);
	}

	// Modification d'un coefficient DCT
	getDCTpos(img, 3, &pos);
	getDCTcoeffValue(img, &pos, &return_value);
	printf("Valeur du coefficient (comp %d, lin %d, col %d, coeff %d) : %d\n", pos.comp, pos.lin, pos.col, pos.coeff, return_value);
	img->dctCoeffs[pos.comp][pos.lin][pos.col][pos.coeff] = 37;

	// Ecriture dans un nouveau fichier
	return_value = jpeg_write_from_coeffs(argv[2], img);
	if (return_value == EXIT_SUCCESS)
		printf("\nImage written in %s\n", argv[2]);
	free_jpeg_img(img);

	return EXIT_SUCCESS;
}
